import { Request, NextFunction, Response } from 'express';

export function userSettings(req: Request, res: Response, next: NextFunction): void {
    if (!req.session) {
        throw new Error('No session object on request object');
    }

    if (!req.session.userSettings) {
        req.session.userSettings = getDefaultSettings();
    }

    req.userSettings = req.session.userSettings;
    next();
}

export function toggleTheme(settings: UserSettings): void {
    if (settings.theme === 'dark') settings.theme = 'light';
    else settings.theme = 'dark';
}

export function toggleNoteOrder(settings: UserSettings): void {
    if (settings.noteOrder === 'ascending') settings.noteOrder = 'descending';
    else settings.noteOrder = 'ascending';
}

function getDefaultSettings(): UserSettings {
    return { ...defaultSettings };
}

const defaultSettings: UserSettings = {
    theme: 'light',
    noteOrder: 'descending',
    noteOrderBy: 'creationDate',
    visibleNotes: 'unfinished'
};
